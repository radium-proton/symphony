package com.cs.symphony;

import java.net.URL;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import com.cs.symphony.model.RoomDetails;
import com.cs.symphony.repo.RoomDetailsRepo;
import com.google.common.collect.Maps;

import authentication.SymBotAuth;
import clients.SymBotClient;
import configuration.SymConfig;
import configuration.SymConfigLoader;
import model.OutboundMessage;
import model.Room;
import model.RoomInfo;
import model.UserInfo;
import services.DatafeedEventsService;

@Component
public class BotStarter {
	private static final Logger LOGGER = LoggerFactory.getLogger(BotStarter.class);

	@Bean
	public SymBotClient botClient() {
		URL url = getClass().getResource("/config.json");
		SymConfigLoader configLoader = new SymConfigLoader();
		SymConfig config = configLoader.loadFromFile(url.getPath());
		SymBotAuth botAuth = new SymBotAuth(config);
		botAuth.authenticate();
		SymBotClient botClient = SymBotClient.initBot(config, botAuth);
		return botClient;

	}

	@Bean
	public DatafeedEventsService dataFeedService(SymBotClient botClient) {
		DatafeedEventsService datafeedEventsService = botClient.getDatafeedEventsService();
		return datafeedEventsService;
	}

	@Bean
	public Map<String, RoomDetails> createRoom(SymBotClient botClient, RoomDetailsRepo repo) {
		Map<String, RoomDetails> roomMap = Maps.newHashMap();
		try {
			List<RoomDetails> roomDetails = repo.findAll();
			for(RoomDetails roomDetail : roomDetails) {
				if(roomDetail.getRoomInfo().getRoomSystemInfo() == null) {
					UserInfo userInfo = botClient.getUsersClient().getUserFromEmail("dvgaba@gmail.com", true);
					
					String imStreamId = botClient.getStreamsClient().getUserIMStreamId(userInfo.getId());
					OutboundMessage message = new OutboundMessage();
					message.setMessage("Welcome");
					botClient.getMessagesClient().sendMessage(imStreamId, message);
					Room room = new Room();
					room.setName(roomDetail.getRoomName());
					room.setDescription(roomDetail.getRoomDesc());
					room.setDiscoverable(roomDetail.getRoomInfo().getRoomAttributes().getDiscoverable());
					room.setPublic(roomDetail.getRoomInfo().getRoomAttributes().getDiscoverable());
					room.setViewHistory(true);
					RoomInfo roomInfo = null;
					roomInfo = botClient.getStreamsClient().createRoom(room);
					LOGGER.info("roomInfo {}", roomInfo);
					roomDetail.setRoomInfo(roomInfo);
					botClient.getStreamsClient().addMemberToRoom(roomInfo.getRoomSystemInfo().getId(), userInfo.getId());
					botClient.getStreamsClient().promoteUserToOwner(roomInfo.getRoomSystemInfo().getId(), userInfo.getId());
					repo.save(roomDetail);
						
				}
				botClient.getStreamsClient().addMemberToRoom(roomDetail.getRoomInfo().getRoomSystemInfo().getId(), botClient.getBotUserInfo().getId());
				for(String member : roomDetail.getMembers()) {
					UserInfo userInfo = botClient.getUsersClient().getUserFromEmail(member, true);
					botClient.getStreamsClient().addMemberToRoom(roomDetail.getRoomInfo().getRoomSystemInfo().getId(), userInfo.getId());
					
				}
				roomMap.put(roomDetail.getRoomName(),roomDetail);
				roomMap.put(roomDetail.getRoomInfo().getRoomSystemInfo().getId(),roomDetail);
			}			
			return roomMap;
		} catch (Exception e) {
			LOGGER.error("{}",e);
			throw new RuntimeException(e);
		}
	}
}
