package com.cs.symphony;

public interface CmdProcessor {
	public void process(MessageContext messageContext);
}
