package com.cs.symphony;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.sun.mail.handlers.message_rfc822;

import clients.SymBotClient;
import listeners.IMListener;
import model.InboundMessage;
import model.OutboundMessage;
import model.Stream;
import services.DatafeedEventsService;

@Component
public class ImListener implements IMListener {

	private SymBotClient botClient;

	@Autowired
	private MessageProcessor messageProcessor;

	@Autowired
	public ImListener(SymBotClient botClient, DatafeedEventsService dataFeedEventService) {
		this.botClient = botClient;
		dataFeedEventService.addIMListener(this);
	}

	public void onIMMessage(InboundMessage inboundMessage) {
		messageProcessor.processIm(inboundMessage);
	}

	public void onIMCreated(Stream stream) {

	}
}
