package com.cs.symphony;

import com.cs.symphony.model.RoomDetails;

import model.InboundMessage;

public class MessageContext {
	private InboundMessage inboundMessage;
	private String cmd;
	private String input;
	private RoomDetails roomDetails;
	
	public RoomDetails getRoomDetails() {
		return roomDetails;
	}
	public void setRoomDetails(RoomDetails roomDetails) {
		this.roomDetails = roomDetails;
	}
	public InboundMessage getInboundMessage() {
		return inboundMessage;
	}
	public void setInboundMessage(InboundMessage inboundMessage) {
		this.inboundMessage = inboundMessage;
	}
	public String getCmd() {
		return cmd;
	}
	public void setCmd(String cmd) {
		this.cmd = cmd;
	}
	public String getInput() {
		return input;
	}
	public void setInput(String input) {
		this.input = input;
	}
	
	
}
