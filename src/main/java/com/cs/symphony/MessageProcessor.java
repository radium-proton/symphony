package com.cs.symphony;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.cs.symphony.model.RoomDetails;

import clients.SymBotClient;
import model.InboundMessage;

@Component
public class MessageProcessor {
	@Autowired
	private SymBotClient botClient;

	@Autowired
	private Map<String, RoomDetails> rooms;
	private static final Logger LOGGER = LoggerFactory.getLogger(MessageProcessor.class);

	private static String pod = "https://develop2.symphony.com/pod";

	public void notifyUser() {
		String url = pod + "/v1/im/create";
		String sessionToken = botClient.getSymAuth().getSessionToken();
		RestTemplate restTemplate = new RestTemplate();
		HttpHeaders headers = new HttpHeaders();
		headers.add("sessionToken", sessionToken);
		HttpEntity<String> requestEntity = new HttpEntity<>(headers);
		Object data = restTemplate.exchange(url, HttpMethod.GET, requestEntity, Object.class);
		LOGGER.info("data {}", data);
	}

	public void getUserId(String emailAddress) {
		String url = pod + "/v3/users?email=" + emailAddress;
		String sessionToken = botClient.getSymAuth().getSessionToken();
		RestTemplate restTemplate = new RestTemplate();
		HttpHeaders headers = new HttpHeaders();
		headers.add("sessionToken", sessionToken);
		HttpEntity<String> requestEntity = new HttpEntity<>(headers);
		Object data = restTemplate.exchange(url, HttpMethod.GET, requestEntity, Object.class);
		LOGGER.info("data {}", data);
	}

	public void process(InboundMessage inboundMessage) {
		MessageContext messageContext = createMessageContext(inboundMessage);
		boolean cmd = isCmd(messageContext);
		LOGGER.info("isCmd {}", cmd);
		LOGGER.info("input {}", messageContext.getInput());
		if (cmd)
			this.processMessage(messageContext);

	}

	@Autowired
	private SkycProcessor skycProcessor;
	@Autowired
	private LegalProcessor legalProcessor;
	@Autowired
	private OnboardingProcessor onboardingProcessor;

	private void processMessage(MessageContext messageContext) {
		CmdProcessor cmdProcessor;
		if ("CS.SKYC".equals(messageContext.getRoomDetails().getRoomName())) {
			cmdProcessor = skycProcessor;
		} else if ("CS.Legal".equals(messageContext.getRoomDetails().getRoomName())) {
			cmdProcessor = legalProcessor;
		} else if ("CS.Onboarding".equals(messageContext.getRoomDetails().getRoomName())) {
			cmdProcessor = onboardingProcessor;
		} else {
			return;
		}
		cmdProcessor.process(messageContext);
	}

	private boolean isCmd(MessageContext messageContext) {
		String message = messageContext.getInboundMessage().getMessageText();
		if (!message.startsWith("/")) {
			return false;
		}
		if (message.indexOf(' ') < 0) {
			return false;
		}
		LOGGER.info("Message Text {}", message);
		String cmd = message.substring(0, message.indexOf(' '));
		String input = message.substring(message.indexOf(' '));
		messageContext.setCmd(cmd);
		messageContext.setInput(input);
		return true;
	}

	private MessageContext createMessageContext(InboundMessage inboundMessage) {
		String roomId = inboundMessage.getStream().getStreamId();
		RoomDetails roomDetails = this.rooms.get(roomId);
		roomDetails = roomDetails == null ? this.rooms.get(NotificationService.Rooms.ONBOARDING) : roomDetails;
		LOGGER.info("Room Name {}", roomDetails.getRoomName());
		MessageContext context = new MessageContext();
		context.setInboundMessage(inboundMessage);
		context.setRoomDetails(roomDetails);
		return context;
	}

	public void processIm(InboundMessage inboundMessage) {
		MessageContext messageContext = createMessageContext(inboundMessage);
		boolean cmd = isCmd(messageContext);
		LOGGER.info("isCmd {}", cmd);
		LOGGER.info("input {}", messageContext.getInput());
		if (cmd) {
			CmdProcessor cmdProcessor;
			cmdProcessor = onboardingProcessor;
			cmdProcessor.process(messageContext);
		}
	}
}
