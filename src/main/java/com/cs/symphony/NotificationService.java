package com.cs.symphony;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cs.symphony.model.RoomDetails;

import clients.SymBotClient;
import model.OutboundMessage;

@Service
public class NotificationService {
	@Autowired
	private SymBotClient botClient;
	@Autowired
	private Map<String, RoomDetails> rooms;

	public enum Rooms {

		KYC("CS.SKYC"),
		ONBOARDING("CS.Onboarding"),
		LEGAL("CS.Legal")
		;
		Rooms(String str) {
			this.roomName = str;
		}

		String roomName;

		public String getRoomName() {
			return roomName;
		}

		public void setRoomName(String roomName) {
			this.roomName = roomName;
		}

	}
	
	public void notifyRoom(Rooms room, String message) {
		OutboundMessage outboundMessage = new OutboundMessage();
		RoomDetails skycGroup = rooms.get(room.getRoomName());
		outboundMessage.setMessage(message);
		this.botClient.getMessagesClient().sendMessage(skycGroup.getRoomInfo().getRoomSystemInfo().getId(),
				outboundMessage);
	}
}
