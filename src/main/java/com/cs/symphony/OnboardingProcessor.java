package com.cs.symphony;

import java.util.Calendar;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cs.symphony.NotificationService.Rooms;
import com.cs.symphony.model.OnboardingRequest;
import com.cs.symphony.model.OnboardingRequest.LegalStatus;
import com.cs.symphony.model.OnboardingRequest.SykyStatus;
import com.cs.symphony.model.RoomDetails;
import com.cs.symphony.repo.OnboardingRequestRepo;

import clients.SymBotClient;
import model.OutboundMessage;

@Component
public class OnboardingProcessor implements CmdProcessor {
	private static final Logger LOGGER = LoggerFactory.getLogger(SkycProcessor.class);
	@Autowired
	private SymBotClient botClient;
	@Autowired
	private Map<String, RoomDetails> rooms;

	@Autowired
	private CounterService counterService;

	@Autowired
	private NotificationService notificationService;

	@Autowired
	private OnboardingRequestRepo onboardingRequestRepo;

	@Override
	public void process(MessageContext messageContext) {
		LOGGER.info("Processing cmd {}", messageContext.getCmd());
		String cmd = messageContext.getCmd();
		switch (cmd) {
		case "/onboard":
			String requestId = "R" + counterService.getNextSequence("request");
			OnboardingRequest onboardingRequest = new OnboardingRequest();
			onboardingRequest.setRequestId(requestId);
			onboardingRequest.setClientName(messageContext.getInput());
			onboardingRequest.setSykcStatus(SykyStatus.STARTED);
			onboardingRequest.setLegalStatus(LegalStatus.NOT_STARTED);
			onboardingRequest.setRtt(false);
			onboardingRequest.setExpectedRtt(Calendar.getInstance().getTime());
			onboardingRequest.setRequestor(messageContext.getInboundMessage().getUser().getEmail());
			onboardingRequestRepo.save(onboardingRequest);
			onboardingRequest.setRequestId(requestId);
			OutboundMessage outboundMessage = new OutboundMessage();
			String message = "Please start skyc process for " + messageContext.getInput() + ", RequestId " + requestId;
			outboundMessage.setMessage(message);
			notificationService.notifyRoom(Rooms.KYC, message);
			outboundMessage.setMessage("Request Created " + requestId);
			this.botClient.getMessagesClient().sendMessage(messageContext.getInboundMessage().getStream().getStreamId(),
					outboundMessage);

			break;
		case "/status":
			onboardingRequest = onboardingRequestRepo.findOne(messageContext.getInput().trim());
			if (onboardingRequest == null) {
				message = "Request not found";
			} else {
				message = onboardingRequest.toString();
			}
			outboundMessage = new OutboundMessage();
			outboundMessage.setMessage(message);
			this.botClient.getMessagesClient().sendMessage(messageContext.getInboundMessage().getStream().getStreamId(),
					outboundMessage);
			break;
		case "/resubmit":
			String userMessage;
			onboardingRequest = onboardingRequestRepo.findOne(messageContext.getInput().trim());
			if (onboardingRequest == null) {
				userMessage = "Request not found";
			} else {
				userMessage = "Request resubmitted";

				if (onboardingRequest.getSykcStatus() != SykyStatus.COMPLETE) {
					onboardingRequest.setSykcStatus(SykyStatus.STARTED);
					onboardingRequestRepo.save(onboardingRequest);
					message = "Please start skyc process for " + messageContext.getInput() + ", RequestId "
							+ messageContext.getInput();
					this.notificationService.notifyRoom(Rooms.KYC, message);
				} else {
					onboardingRequest.setLegalStatus(LegalStatus.STARTED);
					onboardingRequestRepo.save(onboardingRequest);
					message = "Please start Legal process for " + messageContext.getInput() + ", RequestId "
							+ messageContext.getInput();
					this.notificationService.notifyRoom(Rooms.LEGAL, message);
				}
			}
			outboundMessage = new OutboundMessage();
			outboundMessage.setMessage(userMessage);
			this.botClient.getMessagesClient().sendMessage(messageContext.getInboundMessage().getStream().getStreamId(),
					outboundMessage);
			break;
		case "/help":
			message = "/onboard {clientName} /status {reqId} /resubmit {reqId}";
			outboundMessage = new OutboundMessage();
			outboundMessage.setMessage(message);
			this.botClient.getMessagesClient().sendMessage(messageContext.getInboundMessage().getStream().getStreamId(),
					outboundMessage);
			break;
		default:
			return;
		}

	}

}
