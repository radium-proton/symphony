package com.cs.symphony;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import clients.SymBotClient;
import listeners.RoomListener;
import model.InboundMessage;
import model.OutboundMessage;
import model.Stream;
import model.events.RoomCreated;
import model.events.RoomDeactivated;
import model.events.RoomMemberDemotedFromOwner;
import model.events.RoomMemberPromotedToOwner;
import model.events.RoomUpdated;
import model.events.UserJoinedRoom;
import model.events.UserLeftRoom;
import services.DatafeedEventsService;

@Component
public class RoomMessageListener implements RoomListener {

	private SymBotClient botClient;
	private MessageProcessor messsageProcessor;

	@Autowired
	public RoomMessageListener(SymBotClient botClient, DatafeedEventsService dataFeedEventService, MessageProcessor messageProcessor) {
		this.botClient = botClient;
		this.messsageProcessor = messageProcessor;
		dataFeedEventService.addRoomListener(this);
	}

	@SuppressWarnings("unused")
	private final Logger logger = LoggerFactory.getLogger(RoomMessageListener.class);

	public void onRoomMessage(InboundMessage inboundMessage) {
		messsageProcessor.process(inboundMessage);
	}

	public void onRoomCreated(RoomCreated roomCreated) {

	}

	public void onRoomDeactivated(RoomDeactivated roomDeactivated) {

	}

	public void onRoomMemberDemotedFromOwner(RoomMemberDemotedFromOwner roomMemberDemotedFromOwner) {

	}

	public void onRoomMemberPromotedToOwner(RoomMemberPromotedToOwner roomMemberPromotedToOwner) {

	}

	public void onRoomReactivated(Stream stream) {

	}

	public void onRoomUpdated(RoomUpdated roomUpdated) {

	}

	public void onUserJoinedRoom(UserJoinedRoom userJoinedRoom) {
		OutboundMessage messageOut = new OutboundMessage();
		messageOut.setMessage("Welcome " + userJoinedRoom.getAffectedUser().getFirstName() + "!");
		try {
			this.botClient.getMessagesClient().sendMessage(userJoinedRoom.getStream().getStreamId(), messageOut);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void onUserLeftRoom(UserLeftRoom userLeftRoom) {

	}
}
