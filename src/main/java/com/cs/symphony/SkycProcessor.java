package com.cs.symphony;

import java.util.Map;

import javax.ws.rs.core.NoContentException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cs.symphony.model.OnboardingRequest;
import com.cs.symphony.model.OnboardingRequest.LegalStatus;
import com.cs.symphony.model.OnboardingRequest.SykyStatus;
import com.cs.symphony.model.RoomDetails;
import com.cs.symphony.repo.OnboardingRequestRepo;

import clients.SymBotClient;
import exceptions.SymClientException;
import model.OutboundMessage;

@Component
public class SkycProcessor implements CmdProcessor{
	private static final Logger LOGGER = LoggerFactory.getLogger(SkycProcessor.class);
	@Autowired
	private SymBotClient botClient;
	@Autowired
	private Map<String, RoomDetails> rooms;
	@Autowired
	private OnboardingRequestRepo onboardingRequestRepo;
	@Override
	public void process(MessageContext messageContext) {
		LOGGER.info("Processing cmd {}", messageContext.getCmd());
		String cmd = messageContext.getCmd();
		switch(cmd) {
			case "/complete":
				OutboundMessage outboundMessage = new OutboundMessage();
				RoomDetails legalRoom = rooms.get("CS.Legal");
				String message = "Please start legal process for " + messageContext.getInput();
				outboundMessage.setMessage(message);
				OnboardingRequest onboardingRequest = onboardingRequestRepo.findOne(messageContext.getInput().trim());
				if(onboardingRequest!=null){
					onboardingRequest.setSykcStatus(SykyStatus.COMPLETE);
					onboardingRequest.setLegalStatus(LegalStatus.STARTED);
					onboardingRequestRepo.save(onboardingRequest);
				}else {
					message = "Request Not Found";
				}
				
				this.botClient.getMessagesClient().sendMessage(legalRoom.getRoomInfo().getRoomSystemInfo().getId(), outboundMessage);
				break;
			case "/reject":
				outboundMessage = new OutboundMessage();
				message = messageContext.getInput().trim();
				String[] input = message.split(" ");  
				message = "Onboarding Request : " +input[0] +" rejected at KYC stage. Reason : "+input[1];
				outboundMessage.setMessage(message);
				RoomDetails onboardingRoom = rooms.get("CS.Onboarding");
				onboardingRequest = onboardingRequestRepo.findOne(input[0].trim());
				onboardingRequest.setSykcStatus(SykyStatus.REJECTED);
				onboardingRequestRepo.save(onboardingRequest);
			try {
				this.botClient.getMessagesClient().sendMessage(this.botClient.getStreamsClient().getUserIMStreamId(this.botClient.getUsersClient().getUserFromEmail(onboardingRequest.getRequestor(), false).getId()), outboundMessage);
			} catch (SymClientException | NoContentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
				this.botClient.getMessagesClient().sendMessage(onboardingRoom.getRoomInfo().getRoomSystemInfo().getId(), outboundMessage);
				break;
			default: return;	
		}
	}

}
