package com.cs.symphony.model;

import java.util.Date;

import org.springframework.data.annotation.Id;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

public class OnboardingRequest {
	public enum SykyStatus {
		NOT_STARTED, STARTED, COMPLETE, REJECTED;
	}

	public enum LegalStatus {
		NOT_STARTED, STARTED, COMPLETE, REJECTED;
	}
	
	public enum RequestStatus {
		NOT_STARTED, STARTED, COMPLETE, REJECTED;
	}

	@Id
	private String requestId;
	private String clientName;
	private SykyStatus sykcStatus;
	private LegalStatus legalStatus;
	private boolean rtt;
	private Date expectedRtt;
	private String requestor;

	@Override
	public String toString() {
		return new ToStringBuilder(this, ToStringStyle.JSON_STYLE).append("requestId", requestId)
				.append("clientName", clientName).append("sykcStatus", sykcStatus).append("legalStatus", legalStatus)
				.append("rtt", rtt).append("expectedRtt", expectedRtt).append("requestor", requestor).toString();
	}

	public String getRequestor() {
		return requestor;
	}

	public void setRequestor(String requestor) {
		this.requestor = requestor;
	}

	public String getRequestId() {
		return requestId;
	}

	public void setRequestId(String requestId) {
		this.requestId = requestId;
	}

	public String getClientName() {
		return clientName;
	}

	public void setClientName(String clientName) {
		this.clientName = clientName;
	}

	public SykyStatus getSykcStatus() {
		return sykcStatus;
	}

	public void setSykcStatus(SykyStatus sykcStatus) {
		this.sykcStatus = sykcStatus;
	}

	public LegalStatus getLegalStatus() {
		return legalStatus;
	}

	public void setLegalStatus(LegalStatus legalStatus) {
		this.legalStatus = legalStatus;
	}

	public boolean isRtt() {
		return rtt;
	}

	public void setRtt(boolean rtt) {
		this.rtt = rtt;
	}

	public Date getExpectedRtt() {
		return expectedRtt;
	}

	public void setExpectedRtt(Date expectedRtt) {
		this.expectedRtt = expectedRtt;
	}


}
