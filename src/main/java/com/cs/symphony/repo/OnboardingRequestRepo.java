package com.cs.symphony.repo;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.cs.symphony.model.OnboardingRequest;
import com.cs.symphony.model.RoomDetails;

@Repository
public interface OnboardingRequestRepo extends MongoRepository<OnboardingRequest,String>{

}
